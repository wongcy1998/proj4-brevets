# ACP controle times

Current version editor: Toby Wong, chunyauw @ uoregon.edu


## Description

This programme and web-based interface is based upton the ACP controle times.

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

An example of a model based upon, would be the following calculator (https://rusa.org/octime_acp.html).


## AJAX and Flask reimplementation

Difference between this implementation with the above mentioned calculator, is this version will be implemented with AJAX, which will provide updates without posting.


## Execution

In order for this to work, ensure credentials.ini is placed within the /brevets directory.
Shell scripts such as ./runscript.sh and ./rmi_all.sh are here to help you build, run, stop, delete docker images.


## File systems

Within the /brevets directory,
flask_brevets.py is responsible for holding the web client.
acp_times_py is the local calculations units.

Within the /brevets/templates directory,
calc.html is the webpage to display user interface, that includes AJAX.